var expect = require('chai').expect;
var sinon = require('sinon');

var moduleLoader = require('./common/moduleLoader.js');
var mockFactory = require('./common/mockFactory.js');

var js = '../../../apiproxy/resources/jsc/JS-SampleScript.js';



describe('feature: Expiry time adjustment', function() {

    it('should add username to payload', function(done) {
        var mock = mockFactory.getMock();

        console.log(JSON.stringify(mock, null, 4))
        var contextProps = {
            proxyRequest: {
                body: {
                    asJSON: {
                        a: 1
                    }
                }
            },
            flow: "PROXY_REQ_FLOW",
            targetResponse: {
                status: {
                    code: 200
                }
            }
        };

        mock.contextGetVariableMethod.withArgs('response.content').returns("{}");
        mock.withPolicyProperties({'targetProxy.status.code': 500});
        mock.withContextProperties(contextProps);

        moduleLoader.load(js, function(err) {
            expect(err).to.be.undefined;

            expect(mock.contextSetVariableMethod.calledWith('response.content',JSON.stringify({username:"isaias"}, null, 4))).to.be.true;
            done();
        });
    });

});