var expect = require('chai').expect;
var sinon = require('sinon');

var moduleLoader = require('./common/moduleLoader.js');
var mockFactory = require('./common/mockFactory.js');
var exchangeFactory = require('./common/exchangeFactory')

var js = '../../../apiproxy/resources/jsc/JS-HttpClient.js';

describe('feature: Mock HttpClient', function() {

    it('should create Request object', sinon.test(function() {

        var mock = mockFactory.getMock();

        mock.httpClientSendMethod.withArgs(sinon.match({url: 'https://httpbin.org/1?query=something'})).yields("hello hworld 1", null);
        mock.httpClientSendMethod.withArgs(sinon.match({url: 'https://httpbin.org/2'})).yields("hello world 2, well written", null);
        var exchangeObject = exchangeFactory.buildExchangeObj({success: true, waitFor: 2000, response: "hello world"});
        mock.httpClientGetMethod.withArgs('https://httpbin.org/2').returns(exchangeObject);
        // exchangeObject.execute();
        moduleLoader.load(js, function(err) {

            expect(err).to.be.undefined;
            // expect(mock.contextSetVariableMethod.calledWith('response.content',JSON.stringify({username:"isaias"}, null, 4))).to.be.true;
            expect(mock.contextSetVariableMethod.calledWith('response.content',"hello world 2, well written")).to.be.true;
            expect(mock.contextSetVariableMethod.calledWith('response.content',"hello hworld 1")).to.be.true;
            // expect(mock.contextSetVariableMethod.calledWith('response.content',"hello hworld 2")).to.be.true;
            // done();
        });
    }));

});