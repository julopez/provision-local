@api
Feature: API proxy test

    @dev
    Scenario: Recibir respuesta exitosa enviando datos completos
      Given I set Content-Type header to application/xml
      And I pipe contents of file ./target/test/integration/features/fixtures/activation.xml to body
      When I POST to /ProvisioningService/services/v1/activation
      Then response code should be 200
        And response body should contain <code>0<\/code>


    @dev
    Scenario: Recibir respuesta exitosa enviando datos completos
      Given I set Content-Type header to application/xml
      And I pipe contents of file ./target/test/integration/features/fixtures/lineStatusChange.xml to body
      When I POST to /ProvisioningService/services/v1/lineStatusChange
      Then response code should be 200
        And response body should contain <code>0<\/code>

    @dev
    Scenario: Recibir respuesta exitosa enviando datos completos
      Given I set Content-Type header to application/xml
      And I pipe contents of file ./target/test/integration/features/fixtures/newAccount.xml to body
      When I POST to /ProvisioningService/services/v1/newAccount
      Then response code should be 200
        And response body should contain <code>0<\/code>