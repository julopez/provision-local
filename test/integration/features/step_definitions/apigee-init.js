var devappkeys = require('./../../../devAppKeys.json');

exports.init = function (context) {

    devappkeys.forEach(app => {
        if (context.applications[app.name] === undefined) {
            context.applications[app.name] = app;
        } else {
            Object.keys(app).forEach( property => {
                context.applications[app.name][property] = app[property];
            })
        }
    });
};
