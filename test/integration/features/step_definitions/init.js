/* jshint node:true */
'use strict';

var fs = require('fs-extra');
var apickli = require('apickli');

var config = require('../../config.json');
var scheme = config.parameters.scheme || 'https'
var domain = config.parameters.domain;
var basepath = config.parameters.basepath || '/';

console.log("Here I am")
if (fs.pathExistsSync('${project.basedir}/target/test/devAppKeys.json'))  {
    console.log('Loading devAppKeys.json');
    var apigee = require('./apigee-init')
    apigee.init(config);
} else {
    console.log('No devAppKeys.json found!')
}




console.log('api parameters: [' + domain + ', ' + basepath + ']');

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        var tags = [];
        scenario.getTags().forEach(tag => {
            tags.push(tag.getName());
        });
        //this.apickli = new apickli.Apickli(scheme, domain + basepath);
        this.apickli = new apickli.Apickli(scheme, domain);
        this.apickli.addRequestHeader('X-Cucumber-Tags', tags.join(' -> '));
        this.apickli.contexts = config.parameters.contexts;
        Object.keys(config.parameters).forEach(key => {
            this.apickli.storeValueInScenarioScope(key, config.parameters[key]);
        });
        this.apickli.storeValueInScenarioScope('applications', config.applications);
        callback();
    });
};
