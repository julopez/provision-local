const apickli = require('apickli');
const fs = require('fs');
const jsonPath = require('JSONPath');


module.exports = function () {
    this.Given(/^I set variable (.*) to value of path (.*) in (.*) developer app$/, function (variable, path, app, callback) {
        try {
            variable = this.apickli.replaceVariables(variable);
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);

            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});
            this.apickli.storeValueInScenarioScope(variable, result[0]);
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I source variables from path (.*) in (.*) developer app$/, function (path, app, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);
            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});

            Object.keys(result).forEach(key => {
                this.apickli.storeValueInScenarioScope(key, result[key]);
            })
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I source variables from path (.*) in (.*) developer app using prefix (.*)$/, function (path, app, prefix, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);
            prefix = this.apickli.replaceVariables(prefix);

            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});

            Object.keys(result).forEach(key => {
                this.apickli.storeValueInScenarioScope(prefix + '.' + key, result[key]);
            })
            callback();
        } catch (e) {
            callback(e);
        }
    });
}