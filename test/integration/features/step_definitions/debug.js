
module.exports = function () {
    this.Given(/^I print scenario variables$/, function (callback) {
        console.log('---------------------------- BEGIN SCENARIO VARIABLES ----------------------------')
        console.log(this.apickli.scenarioVariables);
        console.log('---------------------------- END SCENARIO VARIABLES ------------------------------')
        callback();
    });

    this.Given(/^I print request body$/, function(callback) {
        console.log('---------------------------- BEGIN REQUEST BODY ----------------------------')
        console.log(this.apickli.requestBody);
        console.log('---------------------------- END REQUEST BODY ------------------------------')
        callback();
    });

    this.Given(/^I print request headers$/, function(callback) {
        console.log('---------------------------- BEGIN REQUEST HEADERS ----------------------------')
        console.log(this.apickli.headers);
        console.log('---------------------------- END REQUEST HEADERS ------------------------------')
        callback();
    });

    this.Then(/^I print response headers$/, function(callback) {
        console.log('---------------------------- BEGIN RESPONSE HEADERS ----------------------------')
        console.log(this.apickli.getResponseObject().headers);
        console.log('---------------------------- END RESPONSE HEADERS ------------------------------')
        callback();
    });
    this.Then(/^I print response body$/, function(callback) {
        console.log('---------------------------- BEGIN RESPONSE BODY ----------------------------')
        console.log(this.apickli.getResponseObject().body);
        console.log('---------------------------- END RESPONSE BODY ------------------------------')
        callback();
    });
}