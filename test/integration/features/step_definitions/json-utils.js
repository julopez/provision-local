'use strict';
var apickli = require('apickli');
const jsonPath = require('JSONPath');
const fs = require('fs');


module.exports = function () {

    this.Then(/^response body as json should have path (.*) equals to (.*)$/, function (path, value2Compare, callback) {

        var path = this.apickli.replaceVariables(path);
        var value2Compare = this.apickli.replaceVariables(value2Compare);

        var body = JSON.parse(this.apickli.getResponseObject().body);
        var json = jsonPath({json: body, path: path, resultType: 'value', autostart:true})[0];

        if (JSON.stringify(json) === JSON.stringify(JSON.parse(value2Compare))) {
            callback();
        } else {
            callback('Objects not equal, received: ' + JSON.stringify(body));
        }
    });

    this.Given(/^I pipe contents of file (.*) to variable (.*)$/, function (file, variable, callback) {

        file = this.apickli.replaceVariables(file);
        variable = this.apickli.replaceVariables(variable);
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                this.apickli.storeValueInScenarioScope(variable, data);
                callback();
            }
        });
    });

    this.Given(/^I set request body path (.*) to (.*)$/, function (path, value, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            value = this.apickli.replaceVariables(value);
            var body = JSON.parse(this.apickli.requestBody);
            var nodes = jsonPath({json: body, path: path, resultType: 'path', autostart: true});
            if (nodes.length > 0) {
                for (var i = 0; i < nodes.length; i++) {
                    var nodePath = JSON.parse(nodes[i]
                        .substring(1)
                        .replace(/'/g, '"')
                        .replace(/]\[/g, ',')).join('.');
                    eval('body.' + nodePath + '=value');
                }
            } else {
                throw 'Could not assing property with path ' + path;
            }
            this.apickli.setRequestBody(JSON.stringify(body));
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I remove request body path (.*)$/, function (path, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            var body = JSON.parse(this.apickli.requestBody);
            var nodes = jsonPath({json: body, path: path, resultType: 'path', autostart: true});
            if (nodes.length > 0) {
                for (var i = 0; i < nodes.length; i++) {
                    var nodePath = JSON.parse(nodes[i]
                        .substring(1)
                        .replace(/'/g, '"')
                        .replace(/]\[/g, ',')).join('.');
                    eval('delete body.' + nodePath);
                }
            } else {
                throw 'Could not remove property with path ' + path;
            }
            this.apickli.setRequestBody(JSON.stringify(body));
            callback();
        } catch (e) {
            callback(e);
        }
    });
}
